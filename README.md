# dotfiles

These are my personal dotfiles that I use on the systems I use. Note that a lot
of the settings are very specific to the particular setups in my daily use and
will probably not work with your particular setup. Feel free to draw inspiration
as you wish, though!


## How to install

* `git clone --bare git@gitlab.com:Spiffyk/dotfiles.git $HOME/.dotfiles-git`
* `wget https://gitlab.com/Spiffyk/dotfiles/-/raw/master/.local/bin/dotfiles -O $HOME/Downloads/dotfiles`
* `chmod +x $HOME/Downloads/dotfiles`
* `$HOME/Downloads/dotfiles checkout` (backup/remove stock files if needed)
* `dotfiles config --local status.showUntrackedFiles no`
* `dotfiles config --local remote.origin.fetch '+refs/heads/*:refs/remotes/origin/*'`
* `dotfiles fetch`
* Install any missing packages - this is very distro-specific, so just look
  through the logs for anything that does not work yet and install away.

## Configuration dependencies

```
bemenu
blueman
bluez
brightnessctl
gammastep
grim
network-manager-applet
noto-fonts-emoji
pamac-tray-icon-plasma
pamixer
playerctl
polkit-gnome
qt6ct
ripgrep
slurp
swappy
swayidle
swaylock
swaync (sway-notification-center)
waybar
wl-clipboard
ttf-nerd-font-symbols
udiskie
xdg-desktop-portal-wlr
xorg-xrandr
```
