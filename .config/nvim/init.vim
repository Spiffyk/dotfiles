" Plugins
call plug#begin('~/.local/share/nvim/site/plugged')

Plug 'junegunn/vim-plug'

Plug 'nvim-lua/plenary.nvim'
Plug 'neovim/nvim-lspconfig'

Plug 'ahmedkhalf/project.nvim'
Plug 'airblade/vim-gitgutter'
Plug 'echasnovski/mini.icons'
Plug 'folke/which-key.nvim'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-cmdline'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/nvim-cmp'
Plug 'hrsh7th/cmp-vsnip'
Plug 'hrsh7th/vim-vsnip'
Plug 'jamessan/vim-gnupg',                      { 'ref': 'f9b608f29003dfde6450931dc0f495a912973a88' }
Plug 'kyazdani42/nvim-web-devicons' ,           { 'ref': '19d257cf889f79f4022163c3fbb5e08639077bd8' }
Plug 'kylechui/nvim-surround',                  { 'ref': 'ec2dc7671067e0086cdf29c2f5df2dd909d5f71f' }
Plug 'lambdalisue/suda.vim',                    { 'ref': 'b97fab52f9cdeabe2bbb5eb98d82356899f30829' }
Plug 'lukas-reineke/indent-blankline.nvim',     { 'ref': 'e7a4442e055ec953311e77791546238d1eaae507' }
Plug 'nvim-telescope/telescope.nvim',           { 'branch': '0.1.x' }
Plug 'sindrets/diffview.nvim',                  { 'ref': '4516612fe98ff56ae0415a259ff6361a89419b0a' }
Plug 'tikhomirov/vim-glsl',                     { 'ref': '40dd0b143ef93f3930a8a409f60c1bb85e28b727' }
Plug 'tpope/vim-fugitive',                      { 'ref': 'd4877e54cef67f5af4f950935b1ade19ed6b7370' }
Plug 'tpope/vim-sleuth',                        { 'ref': 'be69bff86754b1aa5adcbb527d7fcd1635a84080' }
Plug 'tpope/vim-unimpaired',                    { 'ref': '6d44a6dc2ec34607c41ec78acf81657248580bf1' }
Plug 'Spiffyk/unimpaired-which-key.nvim',       { 'branch': 'wk3' }

call plug#end()

packadd termdebug


let mapleader = ","

" Theme
set background=dark
set termguicolors
colorscheme hybrid

" Main
set noswapfile
set number
set secure
set ignorecase
set smartcase
set timeoutlen=200

" Editing
set clipboard=unnamedplus
set completeopt=menu,menuone,noselect
set expandtab
set laststatus=2
set mouse=a
set nowrap
set shiftwidth=0
set tabstop=4
set indentexpr=""
filetype indent off
let g:did_indent_on=1


" Update time (e.g. for Gitgutter)
set updatetime=100

" Show spaces
set fillchars+=diff:⌁
set list
set listchars=tab:⟩\ ,trail:~,nbsp:+
let &showbreak='> '

" Trim spaces
function! Smrk_TrimTrailingSpaces()
    if &filetype =~ 'markdown'
        return
    endif
    %s/\s\+$//e
endfunction
autocmd BufWritePre * call Smrk_TrimTrailingSpaces()

" C++ modules
autocmd BufNewFile,BufRead *.cppm set syntax=cpp

" Markdown fenced languages
let g:markdown_fenced_languages = [ 'html', 'python', 'c', 'bash=sh' ]

" Lua highlighting in vimscript
let g:vimsyn_embed = 'l'

" Wrap line
set colorcolumn=81

" File type and project specifics
function! Sa_Expandtab(mode)
    if exists('b:sleuth') && has_key(b:sleuth['options'], 'expandtab')
        return
    endif

    if a:mode == 1
        setlocal expandtab
    else
        setlocal noexpandtab
    endif
endfunction

function! Sa_Tabstop(val)
    if exists('b:sleuth') && has_key(b:sleuth['options'], 'tabstop')
        return
    endif

    let &l:tabstop = a:val
endfunction

function! Sa_Cinoptions(opts)
    if exists('b:sleuth') && has_key(b:sleuth['options'], 'cinoptions')
        return
    endif

    let &l:cinoptions = a:opts
endfunction

function! ProjectSpecifics()
    let l:path = expand('%:p')

    setlocal indentkeys-=0#
    setlocal formatoptions-=o
    setlocal formatoptions-=c
    setlocal formatoptions-=t
    let ruler = 80

    " Generic C
    if &filetype =~ 'c' || &filetype =~ 'glsl' || &filetype =~ 'zig'
        call Sa_Expandtab(0)
        call Sa_Tabstop(8)
        call Sa_Cinoptions(':0l1g0N-sE-st0')
    endif

    " Knot
    if l:path =~ '/home/spiffyk/Projects/knot'
        if &filetype !~ 'python'
            call Sa_Expandtab(0)
            call Sa_Tabstop(8)
        endif

        if &filetype =~ 'yaml'
            call Sa_Expandtab(1)
            call Sa_Tabstop(2)
        endif
    endif

    if l:path =~ '/home/spiffyk/Projects/knot/knot-resolver/manager'
        let ruler = 110
        call Sa_Expandtab(1)
        call Sa_Tabstop(4)
    endif

    " LibreArp
    if l:path =~ '/home/spiffyk/Projects/librearp'
        let ruler = 120
        call Sa_Expandtab(1)
        call Sa_Tabstop(4)
    endif

    " OpenGothic
    if l:path =~ '/home/spiffyk/Projects/OpenGothic'
        let ruler = 120
        call Sa_Expandtab(1)
        call Sa_Tabstop(2)
    endif

    if &filetype =~ 'text' || &filetype =~ 'md' || &filetype =~ 'markdown' || &filetype =~ 'rst' || &filetype =~ 'tex'
        setlocal formatoptions+=ct
    endif

    if &filetype =~ 'gitcommit'
        let ruler = 72
        setlocal formatoptions+=c
        setlocal formatoptions+=t
    endif

    let &l:colorcolumn = ruler + 1
    let &l:textwidth = ruler

endfunction
call ProjectSpecifics()
autocmd BufReadPost,BufNewFile * call ProjectSpecifics()
autocmd BufReadPost,BufNewFile *.rpz set filetype=dns
autocmd BufReadPost,BufNewFile *.zon set filetype=zig
autocmd BufReadPost,BufNewFile *.lds set filetype=ld

" Forced-type reindent
function! Smrk_Retab()
    let hadet = &expandtab
    if hadet
        set noexpandtab
    endif

    normal ==

    if hadet
        set expandtab
    endif
endfunction

function! Smrk_Respace()
    let hadet = &expandtab
    if ! hadet
        set expandtab
    endif

    normal ==

    if ! hadet
        set noexpandtab
    endif
endfunction

noremap <leader>o :call Smrk_Retab()<CR>
noremap <leader>i :call Smrk_Respace()<CR>

" Scrolloff
function! Smrk_InitReadingMode()
    if &filetype =~ 'qf'
        let b:smrk_default_scrolloff=0
    else
        let b:smrk_default_scrolloff=5
    endif

    let b:smrk_reading_mode=0
    let &l:scrolloff=b:smrk_default_scrolloff
endfunction
autocmd BufReadPost,BufNewFile * call Smrk_InitReadingMode()
call Smrk_InitReadingMode()

function! Smrk_ToggleReadingMode()
    if b:smrk_reading_mode =~ 1
        let b:smrk_reading_mode=0
        let &l:scrolloff=b:smrk_default_scrolloff
        echo 'Centered mode: OFF'
    else
        let b:smrk_reading_mode=1
        let &l:scrolloff=9999
        echo 'Centered mode: ON'
    endif
endfunction
nnoremap <leader>x :call Smrk_ToggleReadingMode()<CR>

" RR Replay
function! Smrk_RrReplay()
    let &l:orig=g:termdebugger
    let g:termdebugger=[ 'rr', 'replay', '--' ]
    Termdebug
    let g:termdebugger=l:orig
endfunction
command Replay call Smrk_RrReplay

" Change directory shortcuts
command Cdp ProjectRoot
command Cdf cd %:s?^\w\+\:\/\/??:p:h

" Aliases
command -nargs=? -complete=file Suda SudaRead <args>

" Mappings - LSP
nnoremap <leader>a :lua vim.lsp.buf.code_action()<CR>
nnoremap <leader>b :lua vim.lsp.buf.definition()<CR>
nnoremap <leader>B :split \| lua vim.lsp.buf.definition()<CR>
nnoremap <leader>q :lua vim.lsp.buf.hover()<CR>
nnoremap <leader>r :lua vim.lsp.buf.rename()<CR>

" Mappings - Diagnostics
nnoremap <leader>dd :lua vim.diagnostic.setloclist()<CR>
nnoremap <leader>dh :lua vim.diagnostic.disable()<CR>
nnoremap <leader>ds :lua vim.diagnostic.enable()<CR>
nnoremap <leader>lq :copen<CR>
nnoremap <leader>ll :lopen<CR>

" Mappings - Diff
nnoremap <leader>dg :diffget<CR>
nnoremap <leader>dp :diffput<CR>
noremap <leader>dg :diffget<CR>
noremap <leader>dp :diffput<CR>
nnoremap <leader>do :DiffviewOpen<CR>
nnoremap <leader>dr :DiffviewRefresh<CR>

" Telescope
nnoremap <leader>fb <cmd>Telescope buffers<CR>
nnoremap <leader>ff <cmd>Telescope find_files hidden=true<CR>
nnoremap <leader>fg <cmd>Telescope live_grep<CR>
nnoremap <leader>fh <cmd>Telescope help_tags<CR>
nnoremap <leader>fr <cmd>Telescope resume<CR>
nnoremap <leader>fs <cmd>Telescope lsp_document_symbols<CR>
nnoremap <leader>fw <cmd>Telescope lsp_dynamic_workspace_symbols<CR>
nnoremap <leader>N  <cmd>Telescope lsp_references<CR>
nnoremap <leader>n  <cmd>Telescope grep_string word_match=-w<CR>

" Stay in visual mode while (de-)indenting
vnoremap > >gv
vnoremap < <gv

" 'n' is forward, 'N' is backward, ALWAYS
noremap <expr> n 'Nn'[v:searchforward]
noremap <expr> N 'nN'[v:searchforward]

" Open config
nnoremap <leader><leader> :tabedit ~/.config/nvim/init.vim<CR>

" Are you absolutely fucking nuts, making the ftplugin format things by default
" and making me search the depths of the internet to disable it? What is this
" bullshit?
let g:zig_fmt_autosave = 0


lua << EOF
--------------------------------------------------------------------------------
-- Project nvim - always cd to project root if found
require("project_nvim").setup({
    detection_methods = { "pattern", "lsp" },
    patterns = { ".git", "_darcs", ".hg", ".bzr", ".svn", ".project_nvim" },
})

-- Diffview
require('diffview').setup {}

-- Autocomplete
local cmp = require('cmp')

cmp.setup({
    snippet = {
        expand = function (args)
            vim.fn["vsnip#anonymous"](args.body)
        end
    },
    mapping = {
        ['<C-Space>'] = cmp.mapping(cmp.mapping.complete(), { 'i', 'c' }),
        ['<C-e>'] = cmp.mapping(cmp.mapping.close(), { 'i', 'c' }),
        ['<Tab>'] = cmp.mapping(cmp.mapping.select_next_item(), { 'i', 'c' }),
        ['<S-Tab>'] = cmp.mapping(cmp.mapping.select_prev_item(), { 'i', 'c' }),
        ['<CR>'] = cmp.mapping(cmp.mapping.confirm({ select = false }), { 'i', 'c' }),
    },
    sources = cmp.config.sources(
        {
            { name = 'nvim_lsp' },
            { name = 'vsnip' },
        },
        {{ name = 'buffer' }}
    )
})

cmp.setup.cmdline('/', {
    sources = cmp.config.sources(
        {{ name = 'nvim_lsp' }},
        {{ name = 'buffer' }}
    )
})

cmp.setup.cmdline(':', {
    sources = cmp.config.sources(
        {{ name = 'path' }},
        {{ name = 'cmdline' }}
    )
})

local on_attach = function(client, bufnr)
    client.server_capabilities.semanticTokensProvider = nil
end
local capabilities = require'cmp_nvim_lsp'.default_capabilities()
local lsp = require'lspconfig'
lsp.clangd.setup{
    capabilities = capabilities,
    on_attach = on_attach,
    root_dir = lsp.util.root_pattern('compile_commands.json', 'build*/compile_commands.json'),
    filetypes = { "c", "cpp", "objc", "objcpp", "cuda", "proto", "ld" },
    on_new_config = function(new_config, new_root_dir)
        globs = {
            "/usr/bin/*gcc",
            "/usr/bin/*g++",
            "/opt/**/bin/*gcc",
            "/opt/**/bin/*g++",
        }
        conc = table.concat(globs, ",")
        table.insert(new_config.cmd, "--query-driver="..conc)
    end
}
lsp.cmake.setup{ -- yay:  cmake-language-server
    capabilities = capabilities,
    on_attach = on_attach,
}
lsp.eslint.setup{ -- npm:  vscode-langservers-extracted
    capabilities = capabilities,
    on_attach = on_attach,
}
lsp.gopls.setup{ -- pacman:  gopls
    capabilities = capabilities,
    on_attach = on_attach,
}
lsp.kotlin_language_server.setup { -- yay:  kotlin-language-server
    capabilities = capabilities,
    on_attach = on_attach,
}
lsp.omnisharp.setup{ -- yay:  omnisharp-roslyn-bin
    capabilities = capabilities,
    on_attach = on_attach,
    cmd = { "dotnet", "/usr/lib/omnisharp/OmniSharp.dll" }
}
lsp.pylsp.setup{ -- pip:  pylsp-rope
    capabilities = capabilities,
    on_attach = on_attach,
    cmd = { "pylsp" }
}
lsp.serve_d.setup{ -- yay:  serve-d
    capabilities = capabilities,
    on_attach = on_attach,
}
lsp.rust_analyzer.setup{ -- pacman:  rust-analyzer
    capabilities = capabilities,
    on_attach = on_attach,
    settings = {
        ["rust-analyzer"] = {
            imports = {
                granularity = {
                    group = "module",
                },
                prefix = "self",
            },
            cargo = {
                buildScripts = {
                    enable = true,
                },
            },
            procMacro = {
                enable = true
            },
        }
    }
}
lsp.zls.setup{ -- pacman:  zls
    capabilities = capabilities,
    on_attach = on_attach,
    settings = {
        enable_autofix = false,
    },
}

-- Telescope
local actions = require'telescope.actions'
local telescope = require'telescope'
telescope.setup {
    defaults = {
        layout_strategy = 'flex',
        layout_config = {
            flex = {
                flip_columns = 130
            }
        },
        mappings = {
            i = {
                ["<LeftMouse>"]       = actions.select_default,
                ["<RightMouse>"]      = actions.close,
                ["<ScrollWheelDown>"] = actions.move_selection_next,
                ["<ScrollWheelUp>"]   = actions.move_selection_previous,
            }
        },
    },
    pickers = {
        live_grep = {
            additional_args = function(opts)
                return {"--hidden"}
            end
        },
    },
}

-- Surround
require'nvim-surround'.setup {}

-- Which key
local wk = require'which-key'
wk.setup {}
wk.add({
    {
        { "<leader>a", desc = "Code action" },
        { "<leader>B", desc = "Go to definition (split)" },
        { "<leader>b", desc = "Go to definition" },
        { "<leader>dd", desc = "Diagnostics loclist" },
        { "<leader>dh", desc = "Hide diagnostics" },
        { "<leader>do", desc = "Diffview open" },
        { "<leader>dr", desc = "Diffview refresh" },
        { "<leader>ds", desc = "Show diagnostics" },
        { "<leader>f", group = "Telescope search" },
        { "<leader>fb", desc = "Buffer list" },
        { "<leader>fc", group = "Git history" },
        { "<leader>fcr", desc = "Git repo history" },
        { "<leader>fcf", desc = "Git file history" },
        { "<leader>ff", desc = "Files by name" },
        { "<leader>fg", desc = "Files by content" },
        { "<leader>fh", desc = "Vim help tags" },
        { "<leader>fr", desc = "Resume last Telescope search" },
        { "<leader>fs", desc = "LSP document symbols" },
        { "<leader>fw", desc = "LSP workspace symbols" },
        { "<leader>h", desc = "GitGutter" },
        { "<leader>l", group = "Location lists" },
        { "<leader>ll", group = "Location list" },
        { "<leader>lq", group = "Quickfix list" },
        { "<leader>n", desc = "Find references" },
        { "<leader>q", desc = "Symbol docs" },
        { "<leader>r", desc = "Rename symbol" },
        { "<leader>x", desc = "Reading mode" },
        { "<leader><leader>", desc = "Open NeoVim config" },
    },

    {
      mode = { "n", "v" },
      { "<leader>d", group = "Diagnostics and Diffmode" },
      { "<leader>dg", desc = "Diff get" },
      { "<leader>dp", desc = "Diff put" },
      { "<leader>i", desc = "Reindent with spaces" },
      { "<leader>o", desc = "Reindent with tabs" },
    },

    {
        mode = { "v" },
        { "<leader>f", group = "Telescope search" },
        { "<leader>fc", desc = "Git line history" },
    },
})

wk.add(require'unimpaired-which-key')

-- Indent blankline
vim.cmd [[highlight IndentBlanklineIndent1 guifg=#282828 gui=nocombine]]
require("ibl").setup({
    indent = {
        char = "│",
        tab_char = "⟩",
        highlight = { "NonText" },
    },
})

local function open_qflist(options)
    vim.fn.setqflist({}, ' ', options)
    vim.api.nvim_command('cfirst')
end


EOF
