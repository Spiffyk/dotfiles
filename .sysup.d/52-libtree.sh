#!/usr/bin/env bash
set -e

libtree_repo="https://github.com/haampie/libtree.git"
libtree_home="$HOME/Projects/libtree"

if [ ! -d "$libtree_home" ]; then
    rm -rf "$libtree_home" # if it is a file for some reason
    git clone "$libtree_repo" "$libtree_home"
fi

cd "$libtree_home"
git pull
make 'CFLAGS=-O3'
