#!/usr/bin/env bash
set -e

zls_repo="https://github.com/zigtools/zls.git"
zls_home="$HOME/Projects/zls"

if [ ! -d "$zls_home" ]; then
    rm -rf "$zls_home" # if it is a file for some reason
    git clone "$zls_repo" "$zls_home"
fi

cd "$zls_home"
git pull
zig build -Doptimize=ReleaseSafe
