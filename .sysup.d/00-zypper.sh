#!/usr/bin/env sh
if ! command -v zypper &> /dev/null; then
    echo "No zypper here."
    exit 0
fi
exec pkexec zypper dup
