#!/usr/bin/env sh
if ! command -v pacman &> /dev/null; then
    echo "No pacman here."
    exit 0
fi
exec pkexec pacman -Syu
