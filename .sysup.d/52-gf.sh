#!/usr/bin/env bash
set -e

gf_repo="https://github.com/nakst/gf.git"
gf_home="$HOME/Projects/gf"

if [ ! -d "$gf_home" ]; then
    rm -rf "$gf_home" # if it is a file for some reason
    git clone "$gf_repo" "$gf_home"
fi

cd "$gf_home"
git pull
./build.sh
