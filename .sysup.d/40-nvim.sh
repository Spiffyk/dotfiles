#!/usr/bin/env sh
set -e

nvim_home="$HOME/.local/nvim"
nvim_shasum_file="$nvim_home/nvim.shasum"
nvim_tarball_url='https://github.com/neovim/neovim/releases/latest/download/nvim-linux64.tar.gz'
nvim_shasum_url='https://github.com/neovim/neovim/releases/latest/download/nvim-linux64.tar.gz.sha256sum'

tmp=$(mktemp --directory '/tmp/sysup-nvim.XXXXXXXX')

pushd "$tmp"
wget "$nvim_shasum_url" -O "nvim.shasum"

dl_nvim=0

if [ ! -e "$nvim_home" ]; then
    echo "'$nvim_home' does not exist - issuing download"
    dl_nvim=1
elif [ -f "$nvim_shasum_file" ]; then
    if [ "$(cat "$nvim_shasum_file")" != "$(cat "nvim.shasum")" ]; then
        echo "nvim shasum mismatch - issuing download"
        dl_nvim=1
    fi
else
    echo "current nvim shasum not found - issuing download"
    dl_nvim=1
fi

if [ "$dl_nvim" -eq "1" ]; then
    wget "$nvim_tarball_url" -O "nvim-linux64.tar.gz"
    tar -xf "nvim-linux64.tar.gz"
    sha256sum --check "nvim.shasum"

    rm -rf "$nvim_home"
    mv 'nvim-linux64' "$nvim_home"
    cp "nvim.shasum" "$nvim_home/nvim.shasum"
else
    echo "Skipping nvim download"
fi
popd

rm -rf "$tmp"

exec nvim -c ":source $HOME/.config/nvim/sysup.vim"
