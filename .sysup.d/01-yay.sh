#!/usr/bin/env sh
if ! command -v yay &> /dev/null; then
    echo "No yay here."
    exit 0
fi
exec yay -Syu --devel
