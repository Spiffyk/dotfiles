#!/usr/bin/env sh
if ! command -v flatpak &> /dev/null; then
    echo "No flatpak here."
    exit 0
fi
exec pkexec flatpak update
