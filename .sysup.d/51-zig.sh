#!/usr/bin/env bash
set -e

pubkey="RWSGOq2NVecA2UPNdBUZykf1CCb147pkmdtYxgb3Ti+JO/wCYvhbAb/U"
dl_index_url="https://ziglang.org/download/index.json"
zig_home_parent="$HOME/.local/zig"
zig_home="$zig_home_parent/nightly"
zig_shasum_file_old="$zig_home_parent/zig.shasum"
zig_shasum_file="$zig_home/zig.shasum"

tmp=$(mktemp --directory '/tmp/sysup-zig.XXXXXXXX')

pushd "$tmp"
wget "$dl_index_url" -O 'index.json'

BAK_IFS="$IFS"
data=($(jq --raw-output '.master["x86_64-linux"] | .tarball, .shasum, .size' < 'index.json'))
IFS="$BAK_IFS"

zig_tarball="${data[0]}"
zig_shasum="${data[1]}"
zig_size="${data[2]}"

echo "zig_tarball: $zig_tarball"
echo "zig_shasum:  $zig_shasum"
echo "zig_size:    $zig_size"

dl_zig=0

if [ ! -e "$zig_home_parent" ]; then
    echo "'$zig_home_parent' does not exist - issuing download"
    mkdir -p "$zig_home_parent"
    dl_zig=1
elif [ -e "$zig_shasum_file_old" ]; then
    echo "Found '$zig_shasum_file_old' - moving to '$zig_home'"
    mv --no-target-directory "$zig_home_parent" "$tmp/zig-old"
    mkdir -p "$zig_home_parent"
    mv --no-target-directory "$tmp/zig-old" "$zig_home"
fi

if [ ! -e "$zig_home" ]; then
    echo "'$zig_home' does not exist - issuing download"
    dl_zig=1
elif [ -f "$zig_shasum_file" ]; then
    if [ "$(cat "$zig_shasum_file")" != "$zig_shasum" ]; then
        echo "zig shasum mismatch - issuing download"
        dl_zig=1
    fi
else
    echo "current zig shasum not found - issuing download"
    dl_zig=1
fi

if [ "$dl_zig" -eq "1" ]; then
    zig_tarball_name="zig.tar.${zig_tarball##*.}"

    wget "$zig_tarball" -O "$zig_tarball_name"
    echo "$zig_shasum $zig_tarball_name" > sha256sum --check

    wget "$zig_tarball.minisig" -O "$zig_tarball_name.minisig"
    minisign -Vm "$zig_tarball_name" -x "$zig_tarball_name.minisig" -P "$pubkey"

    if [ "$(wc --bytes --total only "$zig_tarball_name")" -ne "$zig_size" ]; then
        echo "zig tarball size mismatch"
        exit 1
    fi

    rm -rf "$zig_home"
    tar -xf "$zig_tarball_name"
    mv --no-target-directory 'zig-linux-x86_64-'* "$zig_home"
    echo "$zig_shasum" > "$zig_shasum_file"
else
    echo "Skipping zig download"
fi
popd

rm -rf "$tmp"
